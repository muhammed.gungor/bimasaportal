import React from "react";
import { observer } from "mobx-react";
import { withCookies } from "react-cookie";
import { Form, Input, Button, Checkbox, Layout, Card } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { Container } from 'reactstrap';
import { LoginOutlined } from '@ant-design/icons';

const { Content } = Layout;

const SignIn = observer(
    class SignIn extends React.Component {

        constructor(props) {
            super(props)
            this.state = {

            }
        }
        onFinish = values => {
            console.log('Received values of form: ', values);
        };


        componentDidMount = () => {

        }

        render() {
            const formItemLayout = {
                labelCol: {
                    xs: { span: 24 },
                    sm: { span: 8 },
                },
                wrapperCol: {
                    xs: { span: 24 },
                    sm: { span: 24 },
                },
            };

            const tailFormItemLayout = {
                wrapperCol: {
                    xs: {
                        span: 24,
                        offset: 0,
                    },
                    sm: {
                        span: 24,
                        offset: 0,
                    },
                },
            };


            return <Container style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
                <Content style={{ maxWidth: 360, display: 'flex', flexDirection: 'column', paddingTop: 32 }}>
                    <Card style={{ maxWidth: 360, display: 'flex', flexDirection: 'column', paddingTop: 32, backgroundColor: "#eee0da40", borderColor: "#eee0da40" }} >
                        <Form  {...formItemLayout}
                            name="normal_login"
                            className="login-form"
                            initialValues={{ remember: true }}
                            onFinish={this.onFinish}
                        >
                            <Form.Item
                                name="username"
                                rules={[{ required: true, message: 'Please input your Username!' }]}
                            >
                                <Input prefix={<UserOutlined style={{color:"#cd4532"}} />} placeholder="Username" />
                            </Form.Item>
                            <Form.Item
                                name="password"
                                rules={[{ required: true, message: 'Please input your Password!' }]}
                            >
                                <Input
                                    prefix={<LockOutlined style={{color:"#cd4532"}}/>}
                                    type="password"
                                    placeholder="Password"
                                />
                            </Form.Item>
                            <Form.Item>
                                <Form.Item name="remember" valuePropName="checked" noStyle>
                                    <Checkbox>Remember me</Checkbox>
                                </Form.Item>

                                <a className="login-form-forgot" href="/register">
                                    Forgot password
                                </a>
                            </Form.Item>

                            <Form.Item {...tailFormItemLayout} style={{ display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center" }}>
                                <Button type="default" htmlType="submit" style={{ width:300}} icon={<LoginOutlined />}>
                                    Log in
                                </Button>
                                {/* Or <a href="/register">register now!</a> */}
                            </Form.Item>
                        </Form>
                    </Card>
                </Content>
            </Container>
        }
    }
)

export default withCookies(SignIn);