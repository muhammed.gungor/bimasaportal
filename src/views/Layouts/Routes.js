import React from 'react';


const SignIn = React.lazy(() => import('../Pages/Authentication/SignIn'));
const Register = React.lazy(() => import('../Pages/Authentication/Register'));
const CompanyDashboard = React.lazy(() => import('../Pages/Company/Dashboard'));

const BiMasaRoutes = [
    { path: '/', exact: true, name: 'SignIn', component: SignIn, authenticatedUser:false },
    { path: '/signin', exact: true, name: 'SignIn', component: SignIn, authenticatedUser:false },
    { path: '/register', exact: true, name: 'Register', component: Register, authenticatedUser:false },
    { path: '/dashboard', exact: true, name: 'CompanyDashboard', component: CompanyDashboard, authenticatedUser:true },
]    

export default BiMasaRoutes;