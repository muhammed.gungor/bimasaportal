import React from 'react';
import { Layout, Skeleton } from 'antd';
import { withCookies } from 'react-cookie';
import { Route, Switch } from 'react-router-dom';
import 'antd/dist/antd.css';
import Routes from "../Layouts/Routes";
const { Header,
    Content,
    Footer
} = Layout;

class LandingLayout extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    componentDidMount = () => {
        console.log("LandingLayout");
    }

    render() {
        const loading = () => <div style={{ display: 'flex', justifyContent: 'center', padding: 20 }}><div style={{ width: 560 }}><Skeleton avatar active paragraph={{ rows: 4 }} /></div></div>;
        return <Layout className="layout" >
            <Header style={{ background: '#fff', padding: 0, height: 120 }} >
                <div style={{ display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center" }}>
                    <img alt="logo" src={require('../../Images/logoBimasa.png')} height="120" />
                </div>
            </Header>
            <Content style={{background:'#fff', paddingBottom:"2.5rem"}}>
                <React.Suspense fallback={loading()} >
                    <Switch>
                        {Routes.filter(c => !c.authenticatedUser).map((route, idx) => {
                            return route.component ? (
                                <Route
                                    key={idx}
                                    path={route.path}
                                    exact={route.exact}
                                    name={route.name}
                                    render={props => (
                                        <route.component {...props} />
                                    )} />
                            ) : (null);
                        })}
                    </Switch>
                </React.Suspense>
            </Content>
            <Footer style={{ textAlign: 'center',position:"absolute",bottom:0,width:"100%" }}>Bi'Masa ©{new Date().getFullYear()} Created by MangalSoft</Footer>
        </Layout>
    }
}


export default withCookies(LandingLayout);